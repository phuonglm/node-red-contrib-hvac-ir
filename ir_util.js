//IR support
var IR_Utils = function(){
    this.rawIrCode = function(headerMark, headerSpace, oneMark, oneSpace, zeroMark, zeroSpace, 
            repeatMark, repeatSpace, data, repeateType, repeatTime){
        var result = '';
        if(headerMark && headerMark > 0 && headerSpace && headerSpace > 0){
            result = '' + headerMark + ',' + headerSpace;
        }

        var i,j;
        //var hexDataArray = data.match(/.{1,2}/g);
        for (i = 0; i < data.length; i++){
            var intData = data[i];
            for (j = 0; j <= 7; j++){
                var bitValue = intData & (1 << j);
                var comma = result === '' ? '' : ',';
                if ( bitValue === 0 ){
                    result += comma + zeroMark + ',' + zeroSpace;
                } else {
                    result += comma + oneMark + ',' + oneSpace;
                }
            }
        }

        if(repeatMark && repeatMark > 0 && repeatSpace && repeatSpace > 0){
            result += ',' + repeatMark + ',' + repeatSpace;
        }
        if (repeateType === "wholePackage"){
            for (i = 0; i < repeatTime; i++){
                var items = result.split(',');
                for (j = 0; j < items.length; j++){
                    result += ',' + items[j];
                }
            }
        }

        return result;
    };

    this.pronto2raw = function(pronto){
        pronto = pronto.replace(/ /g,'');
        var buffer = Buffer.from(pronto, 'hex');
        if(buffer.readUInt16BE(0)){
            console.error('Pronto code should start with 0000');
        }
        if (buffer.length/2 != 4*2 + 2 * (buffer.readUInt16BE(2*2) + buffer.readUInt8(2*3))){
            console.error('Number of pulse widths does not match the preamble');
        }
        var frequency = 1 / (buffer.readUInt16BE(1*2) * 0.241246);
        var result = [];
        for(var i=8; i < buffer.length; i+=2){
            result.push(Math.round(buffer.readUInt16BE(i)/frequency))
        }
        return result;
    };

    //this calculate broadlink IR command data from LIRC (raw),
    //this still not enough to total control broadlink since it still need encrypt key,
    //mac address, command type, pulse lengths parameter
    // please read https://github.com/mjg59/python-broadlink/blob/master/protocol.md
    this.raw2broadlink = function(raw_str, base64 = false){
        var pulses = raw_str.split(',');
        var i = 0
        for (i = 0; i < pulses.length; i++){
            pulses[i] = parseInt(pulses[i]);
        }

        var body_buffer = Buffer.alloc(pulses.length*3);
        var header_buffer = Buffer.alloc(4);
        body_buffer.fill(0) // clear all bytes of the buffer
        header_buffer.fill(0) // clear all bytes of the buffer
        header_buffer.writeUInt16BE(0x2600, 0);
        var index = 0;
        
        for (i = 0; i < pulses.length; i++ ){
            var pulse = Math.trunc(pulses[i] * 269 / 8192);
            if(pulse < 256){
                body_buffer.writeUInt8(pulse & 0x00ff, index++);
            } else {
                index++;
                body_buffer.writeUIntBE(pulse >> 8, index++);
                body_buffer.writeUIntBE(pulse & 0x00ff, index++);
            }
            // process.stdout.write(pulse.toString(16) + ',');
        }
        header_buffer.writeUInt16LE(index & 0xffff, 2);
        body_buffer.writeUInt16BE(0x0d05, index);
        index+=2;

        var remainder = (index + 4 + 4) % 16 //body's length +  header's lenght + rm.send_data() adds 4-byte header (02 00 00 00)
        if(remainder){
            index+=(16-remainder)
        }
        body_buffer = body_buffer.slice(0,index);
        var result = Buffer.concat([header_buffer,body_buffer]);
        return(base64 ? result.toString('base64'): result);
    };


    this.broadlink2raw = function(broadlinkInput){
        var pulses = new Buffer.from(broadlinkInput);
        if(pulses.readUInt16BE(0) === 0x2600) {
            var data_length = pulses.readUInt16LE(2) + 4;
            var result = [];
            var i = 0;
            for (i = 4; i < data_length; i++){
                if(pulses.readUInt8(i) === 0){
                    result.push(Math.round(pulses.readUInt16BE(i+1) * 8192 / 269));
                    i+=2;
                } else {
                    result.push(Math.round(pulses.readUInt8(i) * 8192 / 269));
                }
            }
            return(result.join(','));
        } else {
            console.log("Start must be 0x2600")
        }
    };


    this.set_bit = function(data, value, start_index, length){
        var set_single_bit = function(data, value, position){
            var mask = 1 << (position - 1);
            if(value){
                return (data | mask)
            } else {
                return (data & ~mask);
            }
        }
        for (var i=0; i < length; i++){
            var bit = ((value>>i) % 2 !== 0);
            data = set_single_bit(data, bit, start_index - length + i + 1);
        }
        return data;
    };

    this.get_bit = function(data, start_index, length){

        var result = data;
        result = (result << (8 - start_index)) & 0xff ;
        result = (result >> (8 - length)) & 0xff;
        return result;
    };

    this.pulses_to_byte = function(code_config, pulses){
        var result = 0;
        var bit = 0;
        for(var i=0; i< pulses.length; i+=2){
            bit = -1;
            if(pulses[i] && pulses[i+1]){
                if(this.time_precheck([pulses[i]], [code_config.ONE_MARK], code_config.DIFF) && this.time_precheck([pulses[i+1]], [code_config.ONE_SPACE], code_config.DIFF)){
                    bit = 1;
                } else if(this.time_precheck([pulses[i]], [code_config.ZERO_MARK], code_config.DIFF) && this.time_precheck([pulses[i+1]], [code_config.ZERO_SPACE], code_config.DIFF)){
                    bit = 0;
                }
            }

            if(bit == -1){
                console.log('[DEBUG]: Time frame '+ pulses[i] +',' + pulses[i+1] +  ' not matched any binary state.');
            } else {
                result = this.set_bit(result, bit, (i/2)+1,1);
            }
        }
        return result;
    }

    this.time_precheck = function(inputs, valid_inputs, allowed_diff, max_fail_percent){
        allowed_diff = allowed_diff || 7;
        max_fail_percent = max_fail_percent || 0;
        var error_count = 0;
        for(var i = 0; i < inputs.length; i++){
            var valid = false;
            for(var j = 0; j< valid_inputs.length; j++){
                var diff = (Math.abs(inputs[i] - valid_inputs[j]));
                if(diff < allowed_diff){
                    valid = true;
                    break;
                }
            }
            if(!valid){
                error_count++
                // console.log('[utils::time_precheck] invalid input ' + inputs[i]);
                // console.log(valid_inputs.join(','));
            }
            if((error_count/inputs)*100 > max_fail_percent){
                return false;
            }
        }
        return true;
    }

    this.rawToGC = function(freq, repeat, offset, data){
        var dataArray = data.split(',');
        var result = freq + ',' + repeat + ','+ offset;
        for (var i = 0; i < dataArray.length; i++){
            var scale = Math.round(1/freq * 1000000);
            result += ',' + Math.round(parseInt(dataArray[i])/scale)
        }
        return result;
    };

    this.gc_value = function(freq, raw_time){
        return raw_time/(Math.round(1/freq * 1000000));
    };

    this.raw_value = function(freq, gc_time){
        return gc_time*(Math.round(1/freq * 1000000));
    };

    // Config support


    // Debug support

    var padStart = function(string, length, char) {
      //  can be done via loop too:
      //    while (length-- > 0) {
      //      string = char + string;
      //    }
      //  return string;
      return length > 0 ?
        padStart(char + string, --length, char) :
        string;
    }

    this.numToString = function(num, radix, length = num.length) {
      const numString = num.toString(radix);
      return numString.length === length ?
        numString :
        padStart(numString, length - numString.length, "0")
    };

    return {
        rawIrCode: this.rawIrCode,
        set_bit: this.set_bit,
        get_bit: this.get_bit,
        time_precheck: this.time_precheck,
        pulses_to_byte: this.pulses_to_byte,
        rawToGC: this.rawToGC,
        raw2broadlink: this.raw2broadlink,
        broadlink2raw: this.broadlink2raw,
        pronto2raw: this.pronto2raw,
        gc_value: this.gc_value,
        raw_value: this.raw_value,
        numToString: this.numToString
        
    };
}();
module.exports = IR_Utils;
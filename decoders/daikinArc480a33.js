var utils = require("./../ir_util");

function DaikinArc480a33Remote() {
    this.state = [0x11, 0xda, 0x27, 0x00, 0x00, 0x00, 0x00, 0x00,

                  0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0xc5,

                  0x00, 0x08, 0x00];
    this.set('power', 'on');
    this.set('mode','cool');
    this.set('temp', '28');
    this.set('fan','auto');
    this.set('vane','on');
    this.set('powerful', 'off');
}

// Config structure
// [5,1,1] for power
// [5,7,3] mode: 010 for dry, 011 for cool, 110 for fan
// [6,7,6] for temp
// [8,8,4] for fan speed
// [8,4,4] for swing
// [10] for timer
// [13,8,1] for powerful
// [18] for checksum


DaikinArc480a33Remote.prototype = {
    constructor: DaikinArc480a33Remote,
    code_config: {
        HEADER_MARK: 3400,
        HEADER_SPACE: 1750,
        ONE_MARK: 400,
        ONE_SPACE: 1300,
        ZERO_MARK: 400,
        ZERO_SPACE: 650,
        REPEAT_MARK: 400,
        REPEAT_SPACE: 34866,
        FREQ: 38000,
        DIFF: 300,
        PREFIX: '572,260,572,260,572,234,598,260,572,286,572,25272',
        CONST:{
            vendor: {
                _set: function(vendor){

                },
                _get: function(){
                    return 'daikin';
                }
            },
            model: {
                _set: function(model){
                },
                _get: function(){
                    return 'ARC480A33';
                }
            },
            power: {
                _bit_position:[{byte_index: 5, bit_index: 1, bit_length:1}],
                on: [1],
                off: [0]
            },
            mode: {
                _bit_position: [{byte_index: 5, bit_index: 7, bit_length:3}],
                cool: [0b011],
                dry:  [0b010],
                fan:  [0b110]
            },
            fan:{
                _bit_position: [{byte_index: 8, bit_index: 8, bit_length:4}],
                auto: [0b1010],
                1: [0b0011],
                2: [0b0100],
                3: [0b0101],
                4: [0b0110],
                5: [0b0111]
            },
            vane:{
                _bit_position: [{byte_index: 8, bit_index: 4, bit_length:4}],
                on: [0b1111],
                off: [0b0000]
            },

            temp: {
                _set: function(temp) {
                    this.state[6] = utils.set_bit(this.state[6], temp, 7, 6);
                    return this.state;

                },
                _get: function() {
                    return utils.get_bit(this.state[6], 7, 6);
                }
            },
            powerful:{
                _bit_position: [{byte_index: 13, bit_index: 1, bit_length:1}],
                on: [0b1],
                off: [0b0]
            },
        }
    },
    state: [],

    set: function(key, value){
        var bit_configs = this.code_config.CONST;
        if(bit_configs.hasOwnProperty(key)){
            if(bit_configs[key].hasOwnProperty('_set')){
                bit_configs[key]._set.call(this, value);
            } else if(bit_configs[key].hasOwnProperty(value) && bit_configs[key].hasOwnProperty('_bit_position')){
                var bit_positions = bit_configs[key]._bit_position;
                var bit_values = bit_configs[key][value];
                for(var i=0; i < bit_positions.length; i++){
                    if(typeof bit_values[i] != 'undefined'){

                        // console.log('set on key ' + key  + ' attribute ' + value + 
                        //     ' position ' + bit_positions[i].bit_index + 
                        //     ' value ' + utils.numToString(bit_values[i], 2, bit_positions[i].bit_length));

                        var bit_position = bit_positions[i];
                        this.state[bit_position.byte_index] = utils.set_bit(this.state[bit_position.byte_index],
                                                                             bit_values[i],
                                                                             bit_position.bit_index, bit_position.bit_length);
                    }
                }
            } else {
                // console.log('found key "'+ key +'" but attribute "' + value + '" doesn\'t match!!');
            }
        }  else {
            // console.log('key "'+ key +'" not found!!!');
        }

    },
    get: function(key){
        var bit_configs = this.code_config.CONST;
        if(bit_configs.hasOwnProperty(key)){
            if(bit_configs[key].hasOwnProperty('_get')){
                return bit_configs[key]._get.call(this);
            } else if(bit_configs[key].hasOwnProperty('_bit_position')){
                var bit_positions = bit_configs[key]._bit_position;
                for (var attribute_key in bit_configs[key]){
                    if(attribute_key !== '_bit_position'){
                        var bit_values = bit_configs[key][attribute_key];
                        var error = 0;
                        for(var i=0; i < bit_positions.length; i++){
                            if(typeof bit_values[i] != 'undefined'){
                                var bit_position = bit_positions[i];
                                var state_value = utils.get_bit(this.state[bit_position.byte_index],
                                                                    bit_position.bit_index, bit_position.bit_length);
                                // console.log('test on key ' + key  + ' attribute ' + attribute_key + ' number ' + utils.numToString(state_value, 2, bit_position.bit_length));

                                if(state_value !== bit_values[i]){
                                    error++;
                                    break;
                                }
                            }
                        }
                        if(!error){
                            return attribute_key;
                        }
                    }
                }

            }
            // console.log('found key "'+ key +'" but the value doesn\'t match any attribute!!');
        }  else {
            // console.log('key "'+ key +'" not found!!!');
        }
    },
    cal_checksum: function() {
        var sum = 0;
        for (var i = 1; i < 19; i++) {
            this.state[i] &= 0xff;
            sum += this.state[i];
            // console.log('[debug] cal sum byte[' + i + '] value ' + utils.numToString(this.state[i], 16, 2) + ' ' +utils.numToString(sum, 16, 2));
        }
        this.state[18] = sum & 0xff;

        // console.log('[debug] CRC at byte[' + (length - 1) + '] value ' + utils.numToString(this.state[length - 1], 16, 2));
    },
    export: function(type) {
        if(type === 'raw'){
            this.cal_checksum();
            return  this.code_config.PREFIX + ',' + utils.rawIrCode(this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                    this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                    this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                    this.code_config.REPEAT_MARK, this.code_config.REPEAT_SPACE, this.state, undefined, 0);
        } else if (type === 'gc'){
            return utils.rawToGC(this.code_config.FREQ, 1, 1,
                this.export('raw')
            );
        } else if (type === 'broadlink'){
            return utils.raw2broadlink(this.export('raw'));
        } else if (type === 'broadlink_base64'){
            return utils.raw2broadlink(this.export('raw'), true);
        } else if (type === 'debug'){
            var sum = 0;
            var length = this.state.length;
            var message = "Total state length: " + length +'\n';
            message += "I\tHex\tBin\n";
            for (var i = 0; i < length; i++) {
                message += i + '\t' + this.state[i].toString(16) + "\t" + utils.numToString(this.state[i], 2, 8) + '\n';
            }
            return message;
        } else if (type === 'status'){
            var attributes_template = this.code_config.CONST;
            var result = {};
            for (var key in attributes_template){
                if(attributes_template.hasOwnProperty(key)){
                    result[key] = this.get(key);
                }
            }
            return result;
        }
    },


    import: function(type, data) {
        if(type === 'gc' || type === 'raw'){
            var valid_raw_input = [this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                this.code_config.REPEAT_MARK, this.code_config.REPEAT_SPACE
            ];
            this.state = [];
            var that = this;
            var raw_data = [];
            if (type === 'gc') {
                var gc_inputs = data.split(',');

                if (gc_inputs.length > 3) {
                    this.code_config.FREQ = parseInt(gc_inputs[0]);
                    this.code_config.DIFF = utils.raw_value(this.code_config.FREQ, 20);
                    var repeat = parseInt(gc_inputs[1]);
                    var repeat_offset = parseInt(gc_inputs[2]);
                    raw_data = gc_inputs.slice(3).map(function(gc_time) {
                        return utils.raw_value(that.code_config.FREQ, gc_time);
                    });
                } else {
                    console.log('[DEBUG] Pulses too short');
                    return false;
                }
            } else if (type === 'broadlink'){
                raw_data = utils.broadlink2raw(data).split(',');
            } else if (type === 'broadlink_base64'){
                raw_data = utils.broadlink2raw(new Buffer.from(data, 'base64')).split(',');
            } else if (type === 'raw') {
                raw_data = data.split(',');
            }


            if (raw_data.length > 299  && utils.time_precheck(raw_data, valid_raw_input, this.code_config.DIFF, 2) &&
                utils.time_precheck([raw_data[12]], [this.code_config.HEADER_MARK], this.code_config.DIFF, 0) &&
                utils.time_precheck([raw_data[13]], [this.code_config.HEADER_SPACE], this.code_config.DIFF, 0)
                ) {
                var count,i = 0;
                var byte_value;
                for (i = 14; i < 126; i += 16) {
                    byte_value = utils.pulses_to_byte(this.code_config, raw_data.slice(i, i + 16));
                    if(typeof byte_value !== 'undefined'){
                        this.state[count] = byte_value;
                        count++;
                    } else {
                        console.log(raw_data.slice(i, i + 16).join(','));
                    }
                    if (count >= 7) {
                        if (this.state[0] !== 0x11 || this.state[1] !== 0xda ||
                            this.state[2] !== 0x27 || this.state[3] !== 0x00) {
                            console.log('[DEBUG] IR import: bad header');
                            this.state = [];
                            return false;
                        }
                        break;
                    }
                }


                for (i = 126; i < raw_data.length; i += 16) {
                    byte_value = utils.pulses_to_byte(this.code_config, raw_data.slice(i, i + 16));
                    if(typeof byte_value !== 'undefined'){
                        this.state[count] = byte_value;
                        count++;
                    } else {
                        console.log(utils.rawToGC(38000,1,1,raw_data.slice(i, i + 16).join(',')));
                    }
                    if (count >= 19) {
                        break;
                    }
                }

            } else {
                console.log('[DEBUG] IR import: not correct start signals');
                return false;
            }

            return true;
        } else if (type === 'status'){
            DaikinArc480a33Remote.call(this);
            var attributes_template = this.code_config.CONST;
            for (var key in attributes_template){
                if(attributes_template.hasOwnProperty(key) && data.hasOwnProperty(key)){
                    this.set(key, data[key]);
                }
            }
            return true;
        }
        return false;
    }

};
module.exports = DaikinArc480a33Remote;
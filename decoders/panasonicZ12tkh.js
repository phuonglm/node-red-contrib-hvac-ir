var utils = require("./../ir_util");

function PanasonicZ12tkhRemote() {
    this.state = [0x02, 0x20, 0xe0, 0x04, 0x00, 0x00, 0x00, 0x06,
                  0x02, 0x20, 0xe0, 0x04, 0x00, 0x00, 0x00, 0x80,
                  0x00, 0x00, 0x00, 0x0e, 0xe0, 0x00, 0x00 ,0x81,
                  0x00, 0x00, 0x00];
    this.set('model', 'z12tkh');
    this.set('power', 'on');
    this.set('mode','auto');
    this.set('temp', '27');
    // this.set('fan','auto');
    // this.set('vane','auto');
    // this.set('wide_vane','auto');
    // var date = new Date();
    // this.set('clock', [date.getHours(), date.getMinutes()]);
};

PanasonicZ12tkhRemote.prototype = {
    constructor: PanasonicZ12tkhRemote,
    code_config: {
        HEADER_MARK: 3822,
        HEADER_SPACE: 1378,
        ONE_MARK: 754,
        ONE_SPACE: 52,
        ZERO_MARK: 754,
        ZERO_SPACE: 962,
        REPEAT_MARK: 440,
        REPEAT_SPACE: 9734,
        FREQ: 38000,
        DIFF: 300,
        CONST:{
            vendor: {
                _set: function(vendor){

                },
                _get: function(){
                    return 'panasonic';
                }
            },
            model: {
                _set: function(model){
                  // clear & set the various bits and bytes.
                  this.state[13] &= 0xF0;
                  this.state[17] = 0x00;
                  this.state[23] = 0x81;
                  this.state[25] = 0x00;

                    switch (model) {
                        case 'lke':
                            this.state[13] |= 0x02;
                            this.state[17] = 0x06;
                            break;
                        case 'dke':
                            this.state[23] = 0x01;
                            this.state[25] = 0x06;
                            break;
                        case 'nke':
                            this.state[17] = 0x06;
                            break;
                        case 'jke':
                            break;
                        default:
                            break;
                    }
                },
                _get: function(){
                    if (this.state[17] == 0x00 && (this.state[23] & 0x80))
                        return 'jke';
                    if (this.state[17] == 0x06 && (this.state[13] & 0x0F) == 0x02)
                        return 'lke';
                    if (this.state[23] == 0x01 && this.state[25] == 0x06)
                        return 'dke';
                    if (this.state[17] == 0x06)
                        return 'nke';
                    return 'lke';
                }
            },
            power: {
                _bit_position:[{byte_index: 13, bit_index: 1, bit_length:1}],
                on: [1],
                off: [0]
            },
            mode: {
                _bit_position: [{byte_index: 13, bit_index: 6, bit_length:2},{byte_index: 14, bit_index: 8, bit_length:3}],
                auto: [0b00, 0b111],
                cool: [0b11, 0b001],
                dry:  [0b10, 0b001],
                heat: [0b01, 0b001]
            },
            fan:{
                _bit_position: [{byte_index: 16, bit_index: 8, bit_length:4}],
                auto: [0b1010],
                1: [0b0011],
                2:  [0b0101],
                3: [0b0111],
            },
            vane:{
                _bit_position: [{byte_index: 16, bit_index: 4, bit_length:4}],
                auto: [0b1111],
                1: [0b0001],
                2:  [0b0010],
                3: [0b0011],
                4: [0b0100],
                5: [0b0101],
            },
            wide_vane:{
                _bit_position: [{byte_index: 17, bit_index: 4, bit_length:4}],
                auto: [0b1101],
                middle: [0b0110],
                left_end: [0b1001],
                left:  [0b1010],
                right: [0b1011],
                right_end: [0b1010],
            },
            temp: {
                _set: function(temp) {
                    if(utils.get_bit(this.state[14], 8 , 3) == 0b111){
                        if(temp === 'low'){
                            this.state[14] = utils.set_bit(this.state[14], 0b11111, 5, 5);
                        } else if (temp === 'high'){
                            this.state[14] = utils.set_bit(this.state[14], 0b00001, 5, 5);
                        } else {
                            this.state[14] = utils.set_bit(this.state[14], 0b0, 5, 5);
                        }
                    } else if(utils.get_bit(this.state[14], 8 , 3) == 0b001){
                        var set_value = temp - 16;
                        this.state[14] = utils.set_bit(this.state[14], set_value, 5, 4);
                    }
                    return this.state;

                },
                _get: function() {
                    if(utils.get_bit(this.state[14], 8 , 3) == 0b111){
                        var bits = utils.get_bit(this.state[14], 5, 5);
                        if(bits === 0b11111){
                            return 'low';
                        } else if (bits === 0b00001){
                            return 'high';
                        } else {
                            return 'med';
                        }
                    } else if(utils.get_bit(this.state[14], 8 , 3) == 0b001){
                        return utils.get_bit(this.state[14], 5, 4) + 16;
                    }
                }
            },
            extra_mode: {
                nanoe:[],
                econavi: [],
                powerful: []
            }
        }
    },
    state: [],

    set: function(key, value){
        var bit_configs = this.code_config.CONST;
        if(bit_configs.hasOwnProperty(key)){
            if(bit_configs[key].hasOwnProperty('_set')){
                bit_configs[key]._set.call(this, value);
            } else if(bit_configs[key].hasOwnProperty(value) && bit_configs[key].hasOwnProperty('_bit_position')){
                var bit_positions = bit_configs[key]._bit_position;
                var bit_values = bit_configs[key][value];
                for(var i=0; i < bit_positions.length; i++){
                    if(typeof bit_values[i] != 'undefined'){

                        // console.log('set on key ' + key  + ' attribute ' + value + 
                        //     ' position ' + bit_positions[i].bit_index + 
                        //     ' value ' + utils.numToString(bit_values[i], 2, bit_positions[i].bit_length));

                        var bit_position = bit_positions[i];
                        this.state[bit_position.byte_index] = utils.set_bit(this.state[bit_position.byte_index],
                                                                             bit_values[i],
                                                                             bit_position.bit_index, bit_position.bit_length);
                    }
                }
            } else {
                // console.log('found key "'+ key +'" but attribute "' + value + '" doesn\'t match!!');
            }
        }  else {
            // console.log('key "'+ key +'" not found!!!');
        }

    },
    get: function(key){
        var bit_configs = this.code_config.CONST;
        if(bit_configs.hasOwnProperty(key)){
            if(bit_configs[key].hasOwnProperty('_get')){
                return bit_configs[key]._get.call(this);
            } else if(bit_configs[key].hasOwnProperty('_bit_position')){
                var bit_positions = bit_configs[key]._bit_position;
                for (var attribute_key in bit_configs[key]){
                    if(attribute_key !== '_bit_position'){
                        var bit_values = bit_configs[key][attribute_key];
                        var error = 0;
                        for(var i=0; i < bit_positions.length; i++){
                            // console.log('test on key ' + key  + ' attribute ' + attribute_key + ' number ' + i);
                            if(typeof bit_values[i] != 'undefined'){
                                var bit_position = bit_positions[i];
                                var state_value = utils.get_bit(this.state[bit_position.byte_index],
                                                                    bit_position.bit_index, bit_position.bit_length);
                                if(state_value !== bit_values[i]){
                                    error++;
                                    break;
                                }
                            }
                        }
                        if(!error){
                            return attribute_key;
                        }
                    }
                }

            }
            // console.log('found key "'+ key +'" but the value doesn\'t match any attribute!!');
        }  else {
            // console.log('key "'+ key +'" not found!!!');
        }
    },
    cal_checksum: function() {
        var sum = 0;
        var length = this.state.length;
        for (var i = 8; i < length - 1; i++) {
            this.state[i] &= 0xff;
            sum += this.state[i];
            // console.log('[debug] cal sum byte[' + i + '] value ' + utils.numToString(this.state[i], 16, 2) + ' ' +utils.numToString(sum, 16, 2));
        }
        this.state[length - 1] = sum & 0xff;
        // console.log('[debug] CRC at byte[' + (length - 1) + '] value ' + utils.numToString(this.state[length - 1], 16, 2));
    },
    export: function(type) {
        if(type === 'raw'){
            this.cal_checksum();
            return utils.rawIrCode(this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                    this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                    this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                    this.code_config.REPEAT_MARK, this.code_config.REPEAT_SPACE, this.state.slice(0,8), undefined, 0) +
                ',' +
                utils.rawIrCode(this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                    this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                    this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                    this.code_config.REPEAT_MARK, this.code_config.REPEAT_SPACE, this.state.slice(8,27), undefined, 0);
        } else if (type === 'gc'){
            this.cal_checksum();
            return utils.rawToGC(this.code_config.FREQ, 1, 1,
                this.export('raw')
            );
        } else if (type === 'broadlink'){
            return utils.raw2broadlink(this.export('raw'));
        } else if (type === 'broadlink_base64'){
            return utils.raw2broadlink(this.export('raw'), true);
        } else if (type === 'debug'){
            var sum = 0;
            var length = this.state.length;
            var message = "Total state length: " + length +'\n';
            message += "I\tHex\tBin\n";
            for (var i = 0; i < length; i++) {
                message += i + '\t' + this.state[i].toString(16) + "\t" + utils.numToString(this.state[i], 2, 8) + '\n';
            }
            return message;
        } else if (type === 'status'){
            var attributes_template = this.code_config.CONST;
            var result = {};
            for (var key in attributes_template){
                if(attributes_template.hasOwnProperty(key)){
                    result[key] = this.get(key);
                }
            }
            return result;
        }
    },


    import: function(type, data) {
        if(type === 'gc' || type === 'raw'){
            var valid_raw_input = [this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                this.code_config.REPEAT_MARK, this.code_config.REPEAT_SPACE
            ];
            this.state = [];
            var that = this;
            var raw_data = [];
            if (type === 'gc') {
                var gc_inputs = data.split(',');
                if (gc_inputs.length > 3) {
                    this.code_config.FREQ = parseInt(gc_inputs[0]);
                    this.code_config.DIFF = utils.raw_value(this.code_config.FREQ, 20);
                    var repeat = parseInt(gc_inputs[1]);
                    var repeat_offset = parseInt(gc_inputs[2]);
                    raw_data = gc_inputs.slice(3).map(function(gc_time) {
                        return utils.raw_value(that.code_config.FREQ, gc_time);
                    });
                } else {
                    console.log('[DEBUG] Pulses too short');
                    return false;
                }
            } else if (type === 'broadlink'){
                raw_data = utils.broadlink2raw(data).split(',');
            } else if (type === 'broadlink_base64'){
                raw_data = utils.broadlink2raw(new Buffer.from(data, 'base64')).split(',');
            } else if (type === 'raw') {
                raw_data = data.split(',');
            }

            if (raw_data.length > 393 && utils.time_precheck(raw_data, valid_raw_input, this.code_config.DIFF, 2) &&
                utils.time_precheck([raw_data[0], raw_data[132]], [this.code_config.HEADER_MARK], this.code_config.DIFF, 0) &&
                utils.time_precheck([raw_data[1], raw_data[133]], [this.code_config.HEADER_SPACE], this.code_config.DIFF, 0)) {
                var count = 0;
                var byte_value;
                for (var i = 2; i < 130; i += 16) {
                    byte_value = utils.pulses_to_byte(this.code_config, raw_data.slice(i, i + 16));
                    if(typeof byte_value !== 'undefined'){
                        this.state[count] = byte_value;
                        count++;
                    } else {
                        console.log(raw_data.slice(i, i + 16).join(','));
                    }
                    if (count >= 8) {
                        if (this.state[0] !== 0x02 || this.state[1] !== 0x20 ||
                            this.state[2] !== 0xE0 || this.state[3] !== 0x04 ||
                            this.state[7] !== 0x06) {
                            console.log('[DEBUG] IR import: bad header');
                            this.state = [];
                            return false;
                        }
                        break;
                    }
                }

                for (var i = 134; i < raw_data.length; i += 16) {
                    byte_value = utils.pulses_to_byte(this.code_config, raw_data.slice(i, i + 16));
                    if(typeof byte_value !== 'undefined'){
                        this.state[count] = byte_value;
                        count++;
                    } else {
                        console.log(rawToGC(38000,1,1,raw_data.slice(i, i + 16).join(',')));
                    }
                    if (count >= 27) {
                        break;
                    }
                }
                if(this.state[12] === 0x80){
                    this.state = [];
                    console.log('[WARN] Ignored Panasonic special mode');
                    return false;
                }

            } else {
                console.log('[DEBUG] IR import: not correct start signals');
                console.log('[DEBUG] ', raw_data.toString());
                return false;
            }
            return true;
        } else if (type === 'status'){
            PanasonicZ12tkhRemote.call(this);
            var attributes_template = this.code_config.CONST;
            for (var key in attributes_template){
                if(attributes_template.hasOwnProperty(key) && data.hasOwnProperty(key)){
                    this.set(key, data[key]);
                }
            }
            return true;
        }
        return false;
    }

};
module.exports = PanasonicZ12tkhRemote;
var utils = require("./../ir_util");

function MitsubishiRemote() {
    this.state = [0x23, 0xCB, 0x26, 0x01, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00, 0x00];
    this.set('power', 'on');
    this.set('mode','cool');
    this.set('temp', 28);
    this.set('install_position','middle');
    this.set('fan','auto');
    this.set('vane','auto');
    this.set('wide_vane','middle');
    this.set('isee', 'on');
    var date = new Date();
    this.set('clock', [date.getHours(), date.getMinutes()]);
}

MitsubishiRemote.prototype = {
    constructor: MitsubishiRemote,
    branch: "mitsubishi",
    model: 'default',
    code_config: {
        HEADER_MARK: 3400,
        HEADER_SPACE: 1750,
        ONE_MARK: 450,
        ONE_SPACE: 1300,
        ZERO_MARK: 450,
        ZERO_SPACE: 450,
        REPEAT_MARK: 440,
        REPEAT_SPACE: 13800,
        FREQ: 38000,
        DIFF: 187,
        DATA_LENGTH: 18,
        CONST:{
            vendor: {
                _set: function(vendor){

                },
                _get: function(){
                    return 'mitsubishi';
                }
            },
            model: {
                _set: function(model){

                },
                _get: function(){
                    return 'default';
                }
            },
            power: {
                _bit_position:[{byte_index: 5, bit_index: 6, bit_length:1}],
                on: [1],
                off: [0]
            },
            mode: {
                _bit_position: [{byte_index: 6, bit_index: 6, bit_length:3}, {byte_index: 8, bit_index: 3, bit_length:3}],
                auto: [0b000, 0b110],
                cool: [0b011, 0b110],
                dry:  [0b010, 0b010],
                fan:  [0b111, 0b000],
                heat: [0b001, 0b000]
            },
            fan:{
                _set: function(value){
                    if(value === 'auto'){
                        if(utils.get_bit(this.state[9],8,5) === 0b01000 || utils.get_bit(this.state[9],8,5) === 0b10000){
                            this.state[9] = 0b10000000;
                        } else {
                            this.state[9] = utils.set_bit(this.state[9], 0b1000, 7, 4);
                        }
                    } else if(value === 'silent'){
                        this.state[9] = utils.set_bit(this.state[9], 0, 8, 1);
                        this.state[9] = utils.set_bit(this.state[9], 0b000, 3, 3);
                    } else if(typeof value === 'number'){
                        this.state[9] = utils.set_bit(this.state[9], value, 3, 3);
                    }
                },
                _get: function(value){
                    if(utils.get_bit(this.state[9], 3, 3) === 0b000 && utils.get_bit(this.state[9], 8, 1) === 1){
                        return 'auto';
                    } else if(utils.get_bit(this.state[9], 3, 3) === 0 && utils.get_bit(this.state[9], 8, 1) === 0){
                        return 'silent';
                    } else {
                        return utils.get_bit(this.state[9], 3, 3);
                    }
                }
            },
            vane:{
                _set: function(value){
                    if(value === 'auto'){
                        if(utils.get_bit(this.state[9],8,5) === 0b010000 || utils.get_bit(this.state[9],8,5) === 0b10000){
                            this.state[9] = 0b10000000;
                        } else {
                            this.state[9] = utils.set_bit(this.state[9], 0b1000, 7, 4);
                        }
                    } else if(value === 'swing'){
                        this.state[9] = utils.set_bit(this.state[9], 0b1111, 7, 4);
                    } else if(typeof value === 'number'){
                        this.state[9] = utils.set_bit(this.state[9], value, 7, 4);
                    }
                },
                _get: function(value){
                    if(utils.get_bit(this.state[9], 8, 5) === 0b10000 || utils.get_bit(this.state[9], 8, 5) === 0b01000){
                        return 'auto';
                    } else if(utils.get_bit(this.state[9], 7, 4) === 0b1111 || utils.get_bit(this.state[9], 8, 5) === 0b10111){
                        return 'swing';
                    } else {
                        return utils.get_bit(this.state[9], 6, 3);
                    }
                }
            },
            isee: {
                _bit_position:[{byte_index: 6, bit_index: 7, bit_length:1}],
                on: [1],
                off: [0]
            },
            temp: {
                _set: function(temp) {
                    var set_value = temp - 16;
                    this.state[7] = utils.set_bit(this.state[7], set_value, 4, 4);
                    return this.state;
                },
                _get: function() {
                    return utils.get_bit(this.state[7], 4, 4) + 16;
                }
            },
            wide_vane: {
                _bit_position: [{byte_index: 8, bit_index: 8, bit_length:4}],
                left_end: [0b0001],
                left: [0b0010],
                middle: [0b0011],
                right: [0b0100],
                right_end: [0b0101],
                swing: [0b1100]
            },
            area_mode: {
                _bit_position: [{byte_index: 13, bit_index: 8, bit_length:2}],
                swing: [0b00],
                left: [0b01],
                auto: [0b10],
                right: [0b11]
            },
            install_position: {
                _bit_position: [{byte_index: 15, bit_index: 5, bit_length:2}],
                middle: [0b10],
                left: [0b01],
                right: [0b11]
            },
            plasma: {
                _bit_position:[{byte_index: 15, bit_index: 3, bit_length:1}],
                on: [1],
                off: [0]

            },
            clean_mode: {
                _bit_position:[{byte_index: 13, bit_index: 3, bit_length:1}],
                on: [1],
                off: [0]
            },
            clock: {
                _set: function(value){
                    if(value.length > 1){
                        var hour = value[0];
                        var minute = value[1];
                        var currentTime = Math.round((hour + minute / 60) * 6);
                        this.state[10] = utils.set_bit(this.state[10], currentTime, 8, 8);
                    }
                    return this.state;
                }
            }
        }
    },
    state: [],

    set: function(key, value){
        var bit_configs = this.code_config.CONST;
        if(bit_configs.hasOwnProperty(key)){
            if(bit_configs[key].hasOwnProperty('_set')){
                bit_configs[key]._set.call(this, value);
            } else if(bit_configs[key].hasOwnProperty(value) && bit_configs[key].hasOwnProperty('_bit_position')){
                var bit_positions = bit_configs[key]._bit_position;
                var bit_values = bit_configs[key][value];
                for(var i=0; i < bit_positions.length; i++){
                    if(typeof bit_values[i] != 'undefined'){

                        // console.log('set on key ' + key  + ' attribute ' + value + 
                        //     ' position ' + bit_positions[i].bit_index + 
                        //     ' value ' + utils.numToString(bit_values[i], 2, bit_positions[i].bit_length));

                        var bit_position = bit_positions[i];
                        this.state[bit_position.byte_index] = utils.set_bit(this.state[bit_position.byte_index],
                                                                             bit_values[i],
                                                                             bit_position.bit_index, bit_position.bit_length);
                    }
                }
            } else {
                // console.log('found key "'+ key +'" but attribute "' + value + '" doesn\'t match!!');
            }
        }  else {
            // console.log('key "'+ key +'" not found!!!');
        }

    },
    get: function(key){
        var bit_configs = this.code_config.CONST;
        if(bit_configs.hasOwnProperty(key)){
            if(bit_configs[key].hasOwnProperty('_get')){
                return bit_configs[key]._get.call(this);
            } else if(bit_configs[key].hasOwnProperty('_bit_position')){
                var bit_positions = bit_configs[key]._bit_position;
                for ( var attribute_key in bit_configs[key]){
                    if(attribute_key !== '_bit_position'){
                        var bit_values = bit_configs[key][attribute_key];
                        var error = 0;
                        for(var i=0; i < bit_positions.length; i++){
                            // console.log('test on key ' + key  + ' attribute ' + attribute_key + ' number ' + i);
                            if(typeof bit_values[i] != 'undefined'){
                                var bit_position = bit_positions[i];
                                var state_value = utils.get_bit(this.state[bit_position.byte_index],
                                                                                     bit_position.bit_index, bit_position.bit_length);
                                if(state_value !== bit_values[i]){
                                    error++;
                                    break;
                                }
                            }
                        }
                        if(!error){
                            return attribute_key;
                        }
                    }
                }

            }
            // console.log('found key "'+ key +'" but the value doesn\'t match any attribute!!');
        }  else {
            // console.log('key "'+ key +'" not found!!!');
        }
    },
    cal_checksum: function() {
        var sum = 0;
        var length = this.state.length;
        for (var i = 0; i < length - 1; i++) {
            this.state[i] &= 0xff;
            sum += this.state[i];
        }
        this.state[length - 1] = sum & 0xff;
    },
    export: function(type) {
        var result;
        if(type === 'raw'){
            this.cal_checksum();
            return utils.rawIrCode(this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                this.code_config.REPEAT_MARK, this.code_config.REPEAT_SPACE, this.state, "wholePackage", 1);
        } else if(type ==='gc'){
            this.cal_checksum();
            return utils.rawToGC(this.code_config.FREQ, 2, 1,
                utils.rawIrCode(this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                    this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                    this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                    this.code_config.REPEAT_MARK, this.code_config.REPEAT_SPACE, this.state, undefined, 0)
            );
        } else if (type === 'espurna'){
            // New type for espurna 
            result = utils.rawIrCode(this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                this.code_config.REPEAT_MARK, Math.round(this.code_config.REPEAT_SPACE / 1000), this.state, undefined, 0);
            result += ',1,' + this.code_config.FREQ;
            return result;
        } else if (type === 'broadlink'){
            return utils.raw2broadlink(this.export('raw'));
        } else if (type === 'broadlink_base64'){
            return utils.raw2broadlink(this.export('raw'), true);
        } else if (type === 'debug'){
            var length = this.state.length;
            var message = "";
            message += "I\tHex\tBin\n";
            for (var i = 0; i < length; i++) {
                message += i + '\t' + this.state[i].toString(16) + "\t" + utils.numToString(this.state[i], 2, 8) + '\n';
            }
            return message;
        } else if (type === 'status'){
            var attributes_template = this.code_config.CONST;
            result = {};
            for (var key in attributes_template){
                if(attributes_template.hasOwnProperty(key)){
                    result[key] = this.get(key);
                }
            }
            return result;
        }
    },

    import: function(type, data) {
        if(type === 'gc' || type === 'raw'){
            var valid_raw_input = [this.code_config.HEADER_MARK, this.code_config.HEADER_SPACE,
                this.code_config.ONE_MARK, this.code_config.ONE_SPACE,
                this.code_config.ZERO_MARK, this.code_config.ZERO_SPACE,
                this.code_config.REPEAT_MARK, this.code_config.REPEAT_SPACE
            ];
            this.state = [];
            var that = this;
            var raw_data = [];
            if (type === 'gc') {
                var gc_inputs = data.split(',');
                if (gc_inputs.length > 3) {
                    this.code_config.FREQ = parseInt(gc_inputs[0]);
                    this.code_config.DIFF = utils.raw_value(this.code_config.FREQ, 20);
                    var repeat = parseInt(gc_inputs[1]);
                    var repeat_offset = parseInt(gc_inputs[2]);
                    raw_data = gc_inputs.slice(3).map(function(gc_time) {
                        return utils.raw_value(that.code_config.FREQ, gc_time);
                    });
                } else {
                    console.log('[DEBUG] Pulses too short');
                    return false;
                }
            } else if (type === 'broadlink'){
                raw_data = utils.broadlink2raw(data).split(',');
            } else if (type === 'broadlink_base64'){
                raw_data = utils.broadlink2raw(new Buffer.from(data, 'base64')).split(',');
            } else if (type === 'raw') {
                raw_data = data.split(',');
            }
            if (utils.time_precheck(raw_data, valid_raw_input, this.code_config.DIFF, 2) &&
                utils.time_precheck([raw_data[0]], [this.code_config.HEADER_MARK], this.code_config.DIFF, 0) &&
                utils.time_precheck([raw_data[1]], [this.code_config.HEADER_SPACE], this.code_config.DIFF, 0)) {
                var count = 0;
                for (var i = 2; i < raw_data.length; i += 16) {
                    this.state[count] = utils.pulses_to_byte(this.code_config, raw_data.slice(i, i + 16));
                    count++;
                    if (count >= this.code_config.DATA_LENGTH) {
                        break;
                    }

                    if (count > 4 && (this.state[0] !== 0x23 || this.state[1] !== 0xCB ||
                        this.state[2] !== 0x26 || this.state[3] !== 0x01)) {
                        console.log('[DEBUG] IR import: bad header');
                        this.state = [];
                        return false;
                    }
                }
            } else {
                console.log('[DEBUG] IR import: not correct start signals');
                return false;
            }
            return true;
        } else if (type === 'status') {
            MitsubishiRemote.call(this);
            var attributes_template = this.code_config.CONST;
            for (var key in attributes_template){
                if(attributes_template.hasOwnProperty(key) && data.hasOwnProperty(key) && data[key]){
                    this.set(key, data[key]);
                }
            }
            return true;
        }
        return false;
    }

};
module.exports = MitsubishiRemote;
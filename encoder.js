var utils = require("./ir_util");

var remotes = [];
['mitsubishi','panasonic', 'daikin'].forEach(function(model){
    var ir_model = require('./decoders/' + model);
    remotes[model] = new ir_model();
});

module.exports = function(RED) {
    function IREncoder(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
            if(msg.payload && msg.payload.vendor && msg.payload.model && msg.payload.code_type && remotes.hasOwnProperty(msg.payload.vendor)){
                var ir_encoder = remotes[msg.payload.vendor];
                ir_encoder.import('status', msg.payload);

                if(msg.payload.code_type === 'gc' || msg.payload.code_type === 'raw' || 
                    msg.payload.code_type === 'broadlink' || msg.payload.code_type === 'broadlink_base64'){
                    msg.payload.ir_code = ir_encoder.export(msg.payload.code_type);
                }
            }
            node.send(msg);
        });
    }
    RED.nodes.registerType("ac-ir-encoder",IREncoder);
}
var utils = require("./ir_util");

var remotes = [];
['mitsubishi','panasonic', 'daikin'].forEach(function(model){
    var ir_model = require('./decoders/' + model);
    remotes[model] = new ir_model();
});

module.exports = function(RED) {
    function IRDecoder(config) {
        RED.nodes.createNode(this,config);
        var node = this;
        node.on('input', function(msg) {
            var ir_decoder;
            if(msg.payload && msg.payload.code_type && msg.payload.ir_code){
                var found = 0;
                for(var remote_model in remotes){
                    ir_decoder = remotes[remote_model];
                    if(ir_decoder.import(msg.payload.code_type, msg.payload.ir_code)){
                        found++;
                        break;
                    }
                }
                if(found === 0){
                    console.log('[INFO] No model matched this input');
                } else {
                    Object.assign(msg.payload, ir_decoder.export('status'));
                }
            }
            node.send(msg);
        });
    }
    RED.nodes.registerType("ac-ir-decoder", IRDecoder);
}
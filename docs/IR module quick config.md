here is sample config for our ir module configuration. You can set that via USB serial or telnet without going to device's webui. For usb serial just plug device to computer and run `pio device monitor --baud=115200`. Input this command to d

```
set adminPass "<PASSWORD>"
set ssid0 "<wifi_name>"
set pass0 "<wifi_password"
set wifiScan "1"

set mqttEnabled "1"
set mqttServer "<mqtt_server>"
set mqttPort "1883"
set mqttUser "<mqtt_user>"
set mqttPassword "<mqtt_password>"
set telnetSTA "1"
set mqttTopic "<topic_prefix>"
set mqttKeep "300"
set mqttQoS "0"
set mqttRetain "1"
set mqttUseJson "0"
set mqttUseSSL "0"
RESET
```

addition config but not require. Actually we not fully implement it yet. :(

```
set cfg "3"
set wsAuth "1"
set apiEnabled "0"
set apiKey "<apikey_if_api_enabled>"
set apiRealTime "0"
set apiRestFul "1"

set ledMode0 "1"
set dczEnabled "0"
set dczTopicIn "domoticz/in"
set dczTopicOut "domoticz/out"
set haEnabled "0"
set haPrefix "homeassistant"
set boardName "Teracy_IR Bridge"
set ntpDST "1"
set ntpOffset "60"
set ntpRegion "0"
set ntpServer "pool.ntp.org"
set relaySync "0"
set tspkEnabled "0"
set alexaEnabled "0"
set btnDelay "500"
RESET
```
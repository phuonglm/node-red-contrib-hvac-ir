Node RED AC controler
=====================
There are 2 types of node. Encoder and decoder.

Encoder:
--------
Recieve payload from system then return the ir code. The payload will have config depend on vendor, model and feature of HVAC device.
Example:
```
{
    vendor: <vendor name>,
    model: <model name>,
    code_type:gc,
    power: <on|off>,
    mode: <auto|cool|heat|fan|powerful|....>,
    temp: <target temperature>,
    fan: <auto|quite|fan speed>,
    vane: <auto|swing|vane possition>,
    wide_vane: <auto|swing|vane position>,
    .........
}
```
the output will be global cache ir code which can control the HVAC follow that configuration.
```
    {
        ..... <previous attribute above> ....
        code_type: 'gc',
        code: '38000,1,1,xxxxxxxx'
    }
```

Decoder:
--------
Decoder's input ir code from system then depend on payload return the config info. The main reason Decoder exits is to test the encoder and sync the HVAC from user's remote to our home automation system.

```
    Input
    {
        code_type:gc,
        code: '38000,1,1,xxxxxxxx'
    }
```

Decoder will try to match all of it decoder and return first match info:

```
    {
        vendor: <vendor name>,
        model: <model name>,
        code_type:gc,
        power: <on|off>,
        mode: <auto|cool|heat|fan|powerful|....>,
        temp: <target temperature>,
        fan: <auto|quite|fan speed>,
        vane: <auto|swing|vane possition>,
        wide_vane: <auto|swing|vane position>,
        ......
    }
```

Reference
-----
Spec:
[Global Cache IR codes format](https://www.globalcache.com/files/docs/API-iTach.pdf)  
[Mitshubishi HVAC protocol](https://github.com/r45635/HVAC-IR-Control/blob/master/Protocol/Mitsubishi_IR_Packet_Data_v1.1-FULL.pdf)  
[Daikin HVAC IR protocol](http://rdlab.cdmt.vn/project-2013/daikin-ir-protocol)
[Panasonic HVAC IR protocol](https://github.com/r45635/HVAC-IR-Control/blob/master/Protocol/Panasonic%20HVAC%20IR%20Protocol%20specification.pdf)  

Software:
[Node-Red](https://nodered.org/)  
[ESP8266 firmware for IR bridge](https://github.com/xoseperez/espurna/)


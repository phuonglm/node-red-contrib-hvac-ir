var utils = require("./../ir_util");
var mqtt = require('mqtt')

var client;
if( process.env.MQTT_HOST && process.env.MQTT_USER && process.env.MQTT_PASSWORD){
    console.log("Use custom MQTT server " + process.env.MQTT_HOST);
    client  = mqtt.connect(process.env.MQTT_HOST,{username: process.env.MQTT_USER, password: process.env.MQTT_PASSWORD});

} else {
    console.log("Use default MQTT server mqtt://iot.eclipse.org");
    client = mqtt.connect('mqtt://iot.eclipse.org');
}


var remotes = [];
['mitsubishi','panasonic','daikin','daikinArc480a33','panasonicZ12tkh'].forEach(function(model){
    var ir_model = require('./../decoders/' + model);
    remotes[model] = new ir_model();
});

client.on('connect', function () {
    console.log('Connected to server, now subribe to topic /devices/testnode/#');
    client.subscribe('/devices/testnode/#');
})

client.on('message', function (topic, message) {
    // message is Buffer
    if(topic === '/devices/testnode/irout/set' || topic === '/devices/testnode/irin'){
        var found = 0;
        for(var remote_model in remotes){
            console.log('[DEBUG] === Try to import with '+ remote_model +' code format ===')
            if(remotes[remote_model].import('gc', message.toString())){
                console.log('[INFO] Model '+ remote_model);
                console.log(remotes[remote_model].export('debug'));
                console.log(remotes[remote_model].export('status'));
                found++;
            }
        }
        if(found === 0){
            console.log('[INFO] No model matched this input');
        }
    }
})
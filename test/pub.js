var ir = require("./../decoders/daikinArc480a33");
var utils = require("./../ir_util");

var mqtt = require('mqtt')
var client;
if( process.env.MQTT_HOST && process.env.MQTT_USER && process.env.MQTT_PASSWORD){
    console.log("Use custom MQTT server " + process.env.MQTT_HOST);
    client  = mqtt.connect(process.env.MQTT_HOST,{username: process.env.MQTT_USER, password: process.env.MQTT_PASSWORD});

} else {
    console.log("Use default MQTT server mqtt://iot.eclipse.org");
    client = mqtt.connect('mqtt://iot.eclipse.org');
}


var remote = new ir();
remote.set('power','on');
remote.set('mode','cool');
remote.set('temp',25);
remote.set('fan', 3);
remote.set('vane','off');
remote.set('powerful','off');

client.on('connect', function () {
    console.log('Connected to server, now publish content to topic /devices/testnode/irout/set');
    client.subscribe('/devices/testnode/irout/set', function (err) {
        if (!err) {
          client.publish('/devices/testnode/irout/set', remote.export('gc'));
          console.log(remote.export('debug'));
          console.log(remote.export('status'));
          console.log(remote.export('gc'));
          setTimeout(function() {
                process.exit();
          }, 1000);
        }
    });
});

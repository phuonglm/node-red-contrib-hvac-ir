var utils = require("./../ir_util");

var remotes = [];
['mitsubishi','panasonic','daikin'].forEach(function(model){
    var ir_model = require('./../decoders/' + model);
    remotes[model] = new ir_model();
});

var msg = {};
// msg.payload = {  
//    "code_type":"gc",
//    "vendor":"daikin",
//    "model":"default",
//    "power":"on",
//    "mode":"cool",
//    "fan":"auto",
//    "vane":"auto",
//    "isee":"off",
//    "temp":28,
//    "wide_vane":"right_end",
//    "area_mode":"swing",
//    "install_position":"middle",
//    "plasma":"off",
//    "clean_mode":"off"
// };
msg.payload = {  
   "code_type":"broadlink",
   "vendor":"mitsubishi",
   "model":"default",
   "power":"on",
   "mode":"cool",
   "fan":"auto",
   "vane":"on",
   "temp":28,
   "wide_vane":"on",
};
if(msg.payload && msg.payload.vendor && msg.payload.model && 
    msg.payload.code_type && remotes.hasOwnProperty(msg.payload.vendor)){
    var ir_encoder = remotes[msg.payload.vendor];
    ir_encoder.import('status', msg.payload);

    if( msg.payload.code_type === 'gc' || msg.payload.code_type === 'raw' || 
        msg.payload.code_type === 'broadlink'){
        msg.payload.ir_code = ir_encoder.export(msg.payload.code_type);
    }
    console.log(msg.payload.ir_code);
    console.log(ir_encoder.export('status'));
    var input_lirc = [9041, 4507, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 1694, 573, 1694, 573, 1694, 573, 1694, 573, 1694, 573, 1694, 573, 573, 573, 1694, 573, 1694, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 573, 1694, 573, 1694, 573, 1694, 573, 1694, 573, 1694, 573, 1694, 573, 1694, 573, 40906, 9041, 2267, 573, 96193];
    console.log(utils.raw2broadlink(input_lirc.join(','), true));
    console.log(utils.broadlink2raw(utils.raw2broadlink(input_lirc.join(','), false)));
}

